package com.xdev.learndbmovie.presentation

import com.xdev.learndbmovie.data.Result

interface HomeView {

    fun onShowLoading()
    fun onHideLoading()
    fun onResponse(result: List<Result>)
    fun onFailure(error: Throwable)
}