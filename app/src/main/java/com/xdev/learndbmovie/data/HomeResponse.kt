package com.xdev.learndbmovie.data

import com.google.gson.annotations.SerializedName

data class HomeResponse(
    @SerializedName("results")
    val results: List<Result>
)

data class Result(
    @SerializedName("name")
    val title: String,

    @SerializedName("overview")
    val overview: String,

    @SerializedName("poster_path")
    val path: String
)