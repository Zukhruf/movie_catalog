package com.xdev.learndbmovie.di.builder

import com.xdev.learndbmovie.di.module.HomeModule
import com.xdev.learndbmovie.di.scope.Presentation
import com.xdev.learndbmovie.presentation.HomeActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {
    @Presentation
    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun contributeHomeActivity():HomeActivity
}