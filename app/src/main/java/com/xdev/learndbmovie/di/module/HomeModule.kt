package com.xdev.learndbmovie.di.module

import com.xdev.learndbmovie.data.HomeDatasource
import com.xdev.learndbmovie.presentation.HomeActivity
import com.xdev.learndbmovie.presentation.HomePresenter
import com.xdev.learndbmovie.presentation.HomeView
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
abstract class HomeModule{

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun providesHomeDataSource(retrofit: Retrofit) : HomeDatasource =
            retrofit.create(HomeDatasource::class.java)

        @JvmStatic
        @Provides
        fun providesHomePresenter(
            view: HomeView, datasource: HomeDatasource
        ) : HomePresenter = HomePresenter(view, datasource)
    }

    @Binds
    abstract fun bindHomeView(activity: HomeActivity) : HomeView
}