package com.xdev.learndbmovie.di.component

import com.xdev.learndbmovie.BelajarApp
import com.xdev.learndbmovie.di.builder.ActivityBuilder
import com.xdev.learndbmovie.di.module.NetworkModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    NetworkModule::class,
    ActivityBuilder::class])
interface ApplicationComponent : AndroidInjector<BelajarApp>{
}